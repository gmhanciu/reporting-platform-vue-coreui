export default {

    firstLetterCapital(string)
    {
        return string.charAt(0).toUpperCase() + string.slice(1);
    },

    cloneObject(o)
    {
        let out, v, key;
        out = Array.isArray(o) ? [] : {};
        for (key in o) {
            v = o[key];
            out[key] = (typeof v === "object" && v !== null) ? this.cloneObject(v) : v;
        }
        return out;
    },

    treeselectFormat(data, nested)
    {
        if (nested === undefined) {
            nested = false;
        }

        let final = [];
        let templateChild = {
            id: '',
            label: '',
        };

        if (nested)
        {
            let templateParent = {
                id: '',
                label: '',
                children: [],
            };
            let currentParent;
            let parentID = 0;
            // let childID = Object.keys(data).length++;
            for (let group in data)
            {
                currentParent = this.cloneObject(templateParent);
                currentParent.id = 'parent_' + parentID;
                parentID++;
                currentParent.label = group;

                let currentChild;
                let childID = 0;
                for (let child in data[group])
                {
                    currentChild = this.cloneObject(templateChild);
                    currentChild.id = currentParent.id + '_child_' + childID;
                    childID++;
                    currentChild.label = data[group][child];
                    currentParent.children.push(currentChild);
                }

                final.push(currentParent);
            }
        }
        else
        {
            let currentChild;
            let childID = 0;
            for (let child in data)
            {
                currentChild = this.cloneObject(templateChild);
                currentChild.id = 'child_' + childID;
                childID++;
                currentChild.label = data[child];
                final.push(currentChild);
            }
        }

        return final;
    }
}