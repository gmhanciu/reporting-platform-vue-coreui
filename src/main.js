// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import 'core-js/es6/promise'
import 'core-js/es6/string'
import 'core-js/es7/array'
// import cssVars from 'css-vars-ponyfill'
import Vue from 'vue'
import BootstrapVue from 'bootstrap-vue'
import App from './App'
import router from './router'
import store from './store'

import axios from 'axios';
Vue.prototype.$axios = axios;

import helpers from './helpers.js'
Vue.prototype.$Helpers = helpers;

// Global components
import Treeselect from '@riophae/vue-treeselect'
Vue.component('Treeselect', Treeselect);

//Global CSS for global components
import '@riophae/vue-treeselect/dist/vue-treeselect.css'

// todo
// cssVars()

Vue.use(BootstrapVue)

/* eslint-disable no-new */
new Vue({
    store,
    router,
    render: h => h(App)
}).$mount('#app');

// new Vue({
//     el: '#app',
//     store,
//     router,
//     template: '<App/>',
//     components: {
//         App
//     }
// })
