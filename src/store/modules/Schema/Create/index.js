import Selects from './selects'
import Joins from './joins'
import Froms from './froms'
import Wheres from './wheres'
// import axios from 'axios'

const module = {
    namespaced: true,
    modules: {
        Selects,
        Joins,
        Froms,
        Wheres
    },
    state: {
        tables: [],
        columnsByTables: [],
    },
    mutations: {
        getTablesFromDatabase(state, payload)
        {
            state.tables = payload.tables.slice();
        },
        getTablesByColumnsFromDatabase(state, payload)
        {
            state.columnsByTables = this._vm.$Helpers.cloneObject(payload.columnsByTables);
        }
    },
    getters: {
        getTables(state, getters, rootState)
        {
            return state.tables;
        },
        getColumnsByTables(state, getters, rootState)
        {
            return state.columnsByTables;
        }
    },
    actions: {
        getTablesFromDatabase({state, commit, rootState})
        {
            let rootURL = 'http://repapi123.com:8081';
            let tablesURL = rootURL + '/tables';
            let payload = {};

            return this._vm.$axios.get(tablesURL)
                .then( response => {
                    payload.tables = response.data.slice();
                    commit('getTablesFromDatabase', payload);
                });
        },
        getTablesByColumnsFromDatabase({state, commit, rootState})
        {
            let rootURL = 'http://repapi123.com:8081';
            let columnsURL = rootURL + '/columns';
            let payload = {};

            return this._vm.$axios.get(columnsURL)
                .then( response => {
                    payload.columnsByTables = this._vm.$Helpers.cloneObject(response.data);
                    commit('getTablesByColumnsFromDatabase', payload);
                });
        },
    },
};

export default module;