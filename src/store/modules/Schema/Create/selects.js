const module = {
    namespaced: true,
    state: {
        rows: [],
        index: 0,
        templateRow: {
            table: null,
            func: null,
            column: null,
            possibleColumns: null,
            index: null,
            rerenderColumnsTreeselect: 0,
        },
        show: false,
        // validRowsNumber: 0
    },
    getters: {
        getRows (state, getters, rootState) {
            return state.rows;
        },
        getUniqueTables (state, getters, rootState) {
            let tables = state.rows.map(row => row.table);
            return tables.filter((table, index, tables) => (tables.indexOf(table) === index && table !== null)).sort();
        },
        getDisplay (state, getters, rootState) {
            return state.show;
        },
        getRow: (state, getters, rootState) => payload =>
        {
            return state.rows.find(function (row, index) {
                return (row.index === payload.index);
            });
        },
        getPossibleColumns: (state, getters, rootState) => payload =>
        {
            let row = state.rows.find(function (row, index) {
                return (row.index === payload.index);
            });

            return this._vm.$Helpers.treeselectFormat(row.table);
        },
        getRerenderColumnsTreeselect: (state, getters, rootState) => payload =>
        {
            let row = state.rows.find(function (row, index) {
                return (row.index === payload.index);
            });

            return row.rerenderColumnsTreeselect;
        },
        getEmptyRows: (state, getters, rootState) =>
        {
            let currentRows = getters['getRows'];
            return currentRows.filter(function (row, rowIndex) {
                return (row.table === null);
            });
        },
        checkIfNewRowIsNeeded: (state, getters, rootState) =>
        {
            let emptyRows = getters['getEmptyRows'];

            return (emptyRows.length === 0);
        }
        // getValidRowsNumber (state, getters, rootState) {
        //     return state.validRowsNumber;
        // }
    },
    mutations: {
        addEmptyRow(state)
        {
            //clone the array because next actions would also change the template
            let row = this._vm.$Helpers.cloneObject(state.templateRow);
            row.index = state.index;
            state.index++;
            state.rows.push(row);
        },
        removeRow(state, payload)
        {
            state.rows = state.rows.filter(function (row, index) {
                if (row.index !== payload.index)
                {
                    return true;
                }
            });
        },
        removeExtraEmptyRows(state)
        {
            // let oneEmptyRow = false;
            state.rows = state.rows.filter(function (row, index) {

                if (row.table === null)
                {
                    // if (oneEmptyRow === false)
                    // {
                    //     oneEmptyRow = true;
                    //     return row;
                    // }
                    // else
                    // {
                        return false;
                    // }
                }

                return row;
            });
                // .sort((row1, row2) => (row1.index > row2.index) ? 1 : -1);
        },
        changeTable(state, payload)
        {
            state.rows = state.rows.map(function (row, index) {
                if (row.index === payload.index)
                {
                    row.table = payload.table;
                    row.possibleColumns = payload.possibleColumns;
                }
                return row;
            });
        },
        changeFunction(state, payload)
        {
            state.rows = state.rows.map(function (row, index) {
                if (row.index === payload.index)
                {
                    row.func = payload.func;
                }
                return row;
            });
        },
        changeColumn(state, payload)
        {
            state.rows = state.rows.map(function (row, index) {
                if (row.index === payload.index)
                {
                    row.column = payload.column;
                }
                return row;
            });
        },
        displaySection(state)
        {
            state.show = true;
        },
        triggerRerenderColumnsTreeselect(state, payload)
        {
            let row = state.rows.find(function (row, index) {
                return (row.index === payload.index);
            });

            row.rerenderColumnsTreeselect += 1;
        }
        // validRow(state)
        // {
        //     state.validRowsNumber++;
        // },
        // invalidRow(state)
        // {
        //     state.validRowsNumber--;
        // }
    },
    actions: {
        addEmptyRow({state, commit, rootState})
        {
            return new Promise((resolve, reject) => {
                commit('addEmptyRow');
                resolve();
            });
        },
        removeRow({state, commit, rootState}, payload)
        {
            return new Promise((resolve, reject) => {
                commit('removeRow', payload);
                resolve();
            });
        },
        removeExtraEmptyRows({state, commit, getters, rootState})
        {
            return new Promise((resolve, reject) => {
                commit('removeExtraEmptyRows');
                resolve();
            });
        },
        changeTable({state, commit, rootState}, payload)
        {
            return new Promise((resolve, reject) => {
                commit('changeTable', payload);

                // if (state.rows[payload.index].column !== null && state.rows[payload.index].table !== null)
                // {
                //     commit('validRow');
                // }
                // else if (state.validRowsNumber > 0)
                // {
                //     commit('invalidRow');
                // }

                resolve();
            });

        },
        changeFunction({state, commit, rootState}, payload)
        {
            commit('changeFunction', payload);
        },
        changeColumn({state, commit, rootState}, payload)
        {
            commit('changeColumn', payload);
            // state.rows[payload.index].column = payload.column;

            // if (state.rows[payload.index].column !== null && state.rows[payload.index].table !== null)
            // {
            //     commit('validRow');
            // }
            // else if (state.validRowsNumber > 0)
            // {
            //     commit('invalidRow');
            // }
        },
        displaySection({state, commit, rootState})
        {
            commit('displaySection');
        },
        triggerRerenderColumnsTreeselect({state, commit, rootState}, payload)
        {
            return new Promise((resolve, reject) => {
                commit('triggerRerenderColumnsTreeselect', payload);

                // if (state.rows[payload.index].column !== null && state.rows[payload.index].table !== null)
                // {
                //     commit('validRow');
                // }
                // else if (state.validRowsNumber > 0)
                // {
                //     commit('invalidRow');
                // }

                resolve();
            });
        }
    },
};

export default module;
