const module = {
    namespaced: true,
    state: {
        show: false,
        rows: [],
        index: 0,
        templateRow: {
            joinType: null,
            target: null,
            tableFrom: null,
            columnFrom: null,
            rerenderFromColumnsTreeselect: 0,
            tableTo: null,
            columnTo: null,
            rerenderToColumnsTreeselect: 0,
            index: null,
        },
        tables: [],
    },
    mutations: {
        displaySection(state, payload)
        {
            state.show = payload.show;
        },
        addEmptyRow(state)
        {
            //clone the array because next actions would also change the template
            let row = this._vm.$Helpers.cloneObject(state.templateRow);
            row.index = state.index;
            state.index++;
            state.rows.push(row);
        },
        addRow(state, payload)
        {
            let row = this._vm.$Helpers.cloneObject(state.templateRow);
            row.index = state.index;
            state.index++;
            for (let key in payload)
            {
                row.key = payload.key;
            }
            state.rows.push(row);
        },
        removeRow(state, payload)
        {
            state.rows = state.rows.filter(function (row, index) {
                if (row.index !== payload.index)
                {
                    return true;
                }
            });
        },
        changeTable(state, payload)
        {
            // state.rows[payload.index].table = payload.table;
            let rowKey = 'table';
            const capitalLetterType = this._vm.$Helpers.firstLetterCapital(payload.type);

            switch (payload.type)
            {
                case 'from':
                case 'to':
                    rowKey += capitalLetterType;
                    break;
                case 'target':
                    rowKey = 'target';
                    break;
            }
            state.rows[payload.index][rowKey] = payload.table;
        },
        changeFunction(state, payload)
        {
            state.rows[payload.index].func = payload.func;
        },
        changeColumn(state, payload)
        {
            state.rows[payload.index].column = payload.column;
        },
        triggerRerenderFromColumnsTreeselect(state, payload)
        {
            let row = state.rows.find(function (row, index) {
                return (row.index === payload.index);
            });

            row.rerenderFromColumnsTreeselect += 1;
        },
        triggerRerenderToColumnsTreeselect(state, payload)
        {
            let row = state.rows.find(function (row, index) {
                return (row.index === payload.index);
            });

            row.rerenderToColumnsTreeselect += 1;
        },
        removeExtraEmptyRows(state)
        {
            let oneEmptyRow = false;
            state.rows = state.rows.filter(function (row, index) {

                if (row.target === null)
                {
                    if (oneEmptyRow === false)
                    {
                        oneEmptyRow = true;
                        return row;
                    }
                    else
                    {
                        return false;
                    }
                }

                return row;
            });
        },
    },
    getters: {
        getRows (state, getters, rootState) {
            return state.rows;
        },
        getDisplay (state, getters, rootState)
        {
            return state.show;
        },
        getRow: (state, getters, rootState) => payload =>
        {
            return state.rows.find(function (row, index) {
                return (row.index === payload.index);
            });
        },
        getUniqueTables (state, getters, rootState)
        {
            let tablesFrom = state.rows.map(row => row.tableFrom);
            let tablesTo = state.rows.map(row => row.tableTo);

            let allTables = tablesFrom.concat(tablesTo);

            return allTables.filter((table, index, tables) => (tables.indexOf(table) === index && table !== null)).sort();
        },
        getEmptyRows: (state, getters, rootState) =>
        {
            let currentRows = getters['getRows'];
            return currentRows.filter(function (row, rowIndex) {
                return (row.target === null);
            });
        },
        checkIfNewRowIsNeeded: (state, getters, rootState) =>
        {
            let emptyRows = getters['getEmptyRows'];

            return (emptyRows.length === 0);
        }
    },
    actions: {
        displaySection({state, commit, rootState}, payload)
        {
            return new Promise((resolve, reject) => {
                commit('displaySection', payload);
                resolve();
            })
        },
        addEmptyRow({state, commit, rootState})
        {
            return new Promise((resolve, reject) => {
                commit('addEmptyRow');
                resolve();
            });
        },
        addRow({state, commit, rootState}, payload)
        {
            commit('addRow', payload);
        },
        removeRow({state, commit, rootState}, payload)
        {
            commit('removeRow', payload);
        },
        removeExtraEmptyRows({state, commit, getters, rootState})
        {
            return new Promise((resolve, reject) => {
                commit('removeExtraEmptyRows');
                resolve();
            });
        },
        changeTable({state, commit, rootState}, payload)
        {
            commit('changeTable', payload);
        },
        changeFunction({state, commit, rootState}, payload)
        {
            commit('changeFunction', payload);
        },
        changeColumn({state, commit, rootState}, payload)
        {
            commit('changeColumn', payload);
        },
        triggerRerenderFromColumnsTreeselect({state, commit, rootState}, payload)
        {
            return new Promise((resolve, reject) => {
                commit('triggerRerenderFromColumnsTreeselect', payload);
                resolve();
            });
        },
        triggerRerenderToColumnsTreeselect({state, commit, rootState}, payload)
        {
            return new Promise((resolve, reject) => {
                commit('triggerRerenderToColumnsTreeselect', payload);
                resolve();
            });
        }
    },
};

export default module;
