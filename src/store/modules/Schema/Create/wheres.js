const module = {
    namespaced: true,
    state: {
        show: false,
        rows: [],
        templateRow: {
            index: null,
            table: null,
            column: null,
            operation: null,
            tableOperation: null,
            columnOperation: null,
            valueOperation: null,
            rerenderTablesTreeselect: 0,
            rerenderColumnsTreeselect: 0,
            rerenderTablesOperationTreeselect: 0,
            rerenderColumnsOperationTreeselect: 0,
            SQLToggled: false,
        },
        index: 0,
    },
    getters: {
        getDisplay (state, getters, rootState) {
            return state.show;
        },
        getRows (state, getters, rootState) {
            return state.rows;
        },
        getRow: (state, getters, rootState) => payload =>
        {
            return state.rows.find(function (row, index) {
                return (row.index === payload.index);
            });
        },
        getPossibleTables (state, getters, rootState, rootGetters) {
            let selectsTables = rootGetters['Schema/Create/Selects/getUniqueTables'];
            let joinsTables = rootGetters['Schema/Create/Joins/getUniqueTables'];

            let allSelectedTables = selectsTables.concat(joinsTables);

            return allSelectedTables.filter((table, index, tables) => (tables.indexOf(table) === index && table !== null)).sort();
        },
        getEmptyRows: (state, getters, rootState) =>
        {
            let currentRows = getters['getRows'];
            return currentRows.filter(function (row, rowIndex) {
                return (row.table === null);
            });
        },
        checkIfNewRowIsNeeded: (state, getters, rootState) =>
        {
            let emptyRows = getters['getEmptyRows'];

            return (emptyRows.length === 0);
        }
    },
    mutations: {
        displaySection(state)
        {
            state.show = true;
        },
        addEmptyRow(state)
        {
            //clone the array because next actions would also change the template
            let row = this._vm.$Helpers.cloneObject(state.templateRow);
            row.index = state.index;
            state.index++;
            state.rows.push(row);
        },
        triggerRerenderTablesTreeselects(state, payload)
        {
            state.rows.map(function (row, index) {
                // if (row.table !== null)
                // {
                //     if (row.table === payload.previousTable/* || row.table === payload.table*/)
                //     {
                      row.rerenderTablesTreeselect += 1;
                      row.rerenderColumnsTreeselect += 1;
                      row.rerenderTablesOperationTreeselect += 1;
                      row.rerenderColumnsOperationTreeselect += 1;
                    // }
                // }

              return row;
            });
        },
        triggerRerenderColumnsTreeselect(state, payload)
        {
            let row = state.rows.find(function (row, index) {
                return (row.index === payload.index);
            });

            row.rerenderColumnsTreeselect += 1;
        },
        triggerRerenderColumnsOperationTreeselect(state, payload)
        {
            let row = state.rows.find(function (row, index) {
                return (row.index === payload.index);
            });

            row.rerenderColumnsOperationTreeselect += 1;
        },
        changeTable(state, payload)
        {
            state.rows = state.rows.map(function (row, index) {
                if (row.index === payload.index)
                {
                    row.table = payload.table;
                }
                return row;
            });
        },
        changeTableOperation(state, payload)
        {
            state.rows = state.rows.map(function (row, index) {
                if (row.index === payload.index)
                {
                    row.tableOperation = payload.table;
                }
                return row;
            });
        },
        changeColumn(state, payload)
        {
            state.rows = state.rows.map(function (row, index) {
                if (row.index === payload.index)
                {
                    row.column = payload.column;
                }
                return row;
            });
        },
        changeColumnOperation(state, payload)
        {
            state.rows = state.rows.map(function (row, index) {
                if (row.index === payload.index)
                {
                    row.columnOperation = payload.column;
                }
                return row;
            });
        },
        removeRows(state, payload)
        {
            // state.rows = state.rows.filter(function (row, index) {
            //     if (row.table !== null)
            //     {
            //         // if (row.table === payload.previousTable || row.table === payload.table)
            //         if (!payload.allSelectedTables.includes(row.table))
            //         {
            //             return false;
            //         }
            //     }
            //
            //     if (row.tableOperation !== null)
            //     {
            //         if (!payload.allSelectedTables.includes(row.tableOperation))
            //         {
            //             return false;
            //         }
            //     }
            //
            //     return row;
            // });

            state.rows = state.rows.filter(function (row, index) {
                // if (row.table === null && row.tableOperation === null && row.SQLToggled === false)

                //if table is null and sqlToggled is false it means the row is in it's first state and it's empty
                //so it needs to go away
                if (row.table === null && row.SQLToggled === false)
                {
                    return false;
                }

                //if table and tableOperation are both null and sqlToggled is true it means the row
                //is in the first empty state of an SQL row
                //but since this state is different than the initial state (where sqlToggled is false)
                //in this particular case, the row won't get removed
                if (row.table === null && row.tableOperation === null && row.SQLToggled === true)
                {
                    return row;
                }

                return row;
            });
        },
        updateRows(state, payload)
        {
            state.rows = state.rows.map(function (row, index) {
                if (row.table !== null)
                {
                    // if (row.table === payload.previousTable || row.table === payload.table)
                    if (!payload.allSelectedTables.includes(row.table))
                    {
                        row.table = null;
                        row.column = null;
                    }
                }

                if (row.tableOperation !== null)
                {
                    if (!payload.allSelectedTables.includes(row.tableOperation))
                    {
                        row.tableOperation = null;
                        row.columnOperation = null;
                    }
                }

                return row;
            });
        },
        changeOperation(state, payload)
        {
            state.rows = state.rows.map(function (row, index) {
                if (row.index === payload.index)
                {
                    row.operation = payload.operation;
                }
                return row;
            });
        },
        changeSQLToggled(state, payload)
        {
            state.rows = state.rows.map(function (row, index) {
                if (row.index === payload.index)
                {
                    row.SQLToggled = payload.SQLToggled;

                    if (payload.SQLToggled === true)
                    {
                        row.valueOperation = null;
                    }
                    else
                    {
                        row.tableOperation = null;
                        row.columnOperation = null;
                    }
                }
                return row;
            });
        },
        removeExtraEmptyRows(state)
        {
            // let oneEmptyRow = false;
            state.rows = state.rows.filter(function (row, index) {

                if (row.table === null)
                {
                    // if (oneEmptyRow === false)
                    // {
                    //     oneEmptyRow = true;
                    //     return row;
                    // }
                    // else
                    // {
                        return false;
                    // }
                }

                return row;
            });
        },
    },
    actions: {
        changeTable({state, commit, rootState}, payload)
        {
            return new Promise((resolve, reject) => {
                commit('changeTable', payload);
                resolve();
            });
        },
        changeTableOperation({state, commit, rootState}, payload)
        {
            return new Promise((resolve, reject) => {
                commit('changeTableOperation', payload);
                resolve();
            });
        },
        changeColumn({state, commit, rootState}, payload)
        {
            commit('changeColumn', payload);
            // state.rows[payload.index].column = payload.column;

            // if (state.rows[payload.index].column !== null && state.rows[payload.index].table !== null)
            // {
            //     commit('validRow');
            // }
            // else if (state.validRowsNumber > 0)
            // {
            //     commit('invalidRow');
            // }
        },
        changeColumnOperation({state, commit, rootState}, payload)
        {
            commit('changeColumnOperation', payload);
            // state.rows[payload.index].column = payload.column;

            // if (state.rows[payload.index].column !== null && state.rows[payload.index].table !== null)
            // {
            //     commit('validRow');
            // }
            // else if (state.validRowsNumber > 0)
            // {
            //     commit('invalidRow');
            // }
        },
        addEmptyRow({state, commit, rootState})
        {
            return new Promise((resolve, reject) => {
                commit('addEmptyRow');
                resolve();
            })
        },
        removeExtraEmptyRows({state, commit, getters, rootState})
        {
            return new Promise((resolve, reject) => {
                commit('removeExtraEmptyRows');
                resolve();
            });
        },
        displaySection({state, commit, rootState}, payload)
        {
            return new Promise((resolve, reject) => {
                commit('displaySection', payload);
                resolve();
            })
        },
        triggerRerenderTablesTreeselects({state, commit, getters, rootState}, payload)
        {
            return new Promise((resolve, reject) => {
                // payload.possibleTables = getters.getPossibleTables;
                commit('triggerRerenderTablesTreeselects', payload);
                resolve();
            })
        },
        // removeRows({state, commit, rootState, rootGetters}, payload)
        removeRows({state, commit, rootState, rootGetters})
        {
            return new Promise((resolve, reject) => {
                let payload = {};
                payload.allSelectedTables = rootGetters['Schema/Create/Selects/getUniqueTables'];
                commit('removeRows', payload);
                resolve();
            })
        },
        updateRows({state, commit, rootState, rootGetters})
        {
            return new Promise((resolve, reject) => {
                let payload = {};
                payload.allSelectedTables = rootGetters['Schema/Create/Selects/getUniqueTables'];
                commit('updateRows', payload);
                resolve();
            })
        },
        triggerRerenderColumnsTreeselect({state, commit, rootState}, payload)
        {
            commit('triggerRerenderColumnsTreeselect', payload);
        },
        triggerRerenderColumnsOperationTreeselect({state, commit, rootState}, payload)
        {
            commit('triggerRerenderColumnsOperationTreeselect', payload);
        },
        changeOperation({state, commit, rootState}, payload)
        {
            commit('changeOperation', payload);
        },
        changeSQLToggled({state, commit, rootState}, payload)
        {
            commit('changeSQLToggled', payload);
        }
    },
};

export default module;
