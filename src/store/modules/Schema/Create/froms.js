const module = {
    namespaced: true,
    state: {
        show: false,
        tables: [],
    },
    getters: {
        getDisplay (state, getters, rootState)
        {
            return state.show;
        },
        getTables (state, getters, rootState)
        {
            return state.tables;
        }
    },
    mutations: {
        update (state, payload)
        {
            state.show = payload.show;
            state.tables = payload.tables;
        },
        // displaySection(state, payload)
        // {
        //     state.show = payload.show;
        // },
        // setTables(state, payload)
        // {
        //     state.tables = payload.tables;
        // }
    },
    actions: {
        update ({state, commit, getters, rootState, rootGetters})
        {
            return new Promise((resolve, reject) => {
                const uniqueTables = rootGetters['Schema/Create/Selects/getUniqueTables'];
                const payload = {
                    show: (uniqueTables.length > 1),
                    tables: uniqueTables,
                };
                commit('update', payload);
                resolve();
            })
            // commit('setTables', payload);
            // commit('displaySection', payload);
        }
    },
};

export default module;