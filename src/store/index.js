import Vue from 'vue'
import Vuex from 'vuex'

// import CreateSchema from './modules/Schema/Create/index.js'

Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        // CreateSchema
    },
    state: {

    },
    getters: {

    },
    mutations: {

    },
    actions: {

    },
});